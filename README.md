# vpnwatch
VPN Watching bash script for use with OpenVPN on Openwrt

 Standard bash script, requires bash install on OpenWrt
 and monit to be installed and configured. Monit is a 
 system monitoring software that also trigger actions
 as a result of certain events. In my case monit was being
 used to ping the domain in this script and on potential
 failure this bash script was invoked. You could alternatively
 also use cron to trigger a check periodically. 

 We assume a few things when this is called, first
 that the VPN DNS (Expressvpn) is failing second
 that a restart of openvpn will fix it

 Note the location of OpenVPN startup script assuming
 its already setup and there

 Note location of startup script is /etc/init.d/openvpn
 We assume the OpenVPN file used to configure this connection
 also has script security set to 2 so we can use the up 
 and down scripts to force the DNS to swap. 

 Make sure your wan port isn't setup for DHCP as some DHCP
 setups can cause the DNS to reset from time to time 
 Your ISP's DHCP may not be a problem though as I only
 encountered this when I had another router upstream

